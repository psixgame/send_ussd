package com.gmail.psixgame.sendussd.utils.time;

/**
 * Created by ZOG on 6/26/2017.
 */

public interface ITimeProvider {
    long getCurrentTime();
}
