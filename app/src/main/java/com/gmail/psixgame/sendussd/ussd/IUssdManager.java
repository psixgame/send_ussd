package com.gmail.psixgame.sendussd.ussd;

/**
 * Created by ZOG on 5/12/2017.
 */

public interface IUssdManager {

    boolean isUssdRequestRunning();
    void sendUssd(UssdMessage _message, UssdCallback _callback);

}
