package com.gmail.psixgame.sendussd.utils.permission;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.gmail.psixgame.sendussd.ussd.UssdAccessibilityService;

/**
 * Created by ZOG on 5/23/2017.
 */

public final class PermissionHelperImpl implements IPermissionHelper {

    private static final String TAG = PermissionHelperImpl.class.getSimpleName();

    @Override
    public final boolean isEnabledInAccessibilitySetting(final Context _context) {
        int accessibilityEnabled = 0;
        final String service = _context.getPackageName() + "/" + UssdAccessibilityService.class.getCanonicalName();
        try {
            accessibilityEnabled = Settings.Secure.getInt(
                    _context.getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            Log.v(TAG, "accessibilityEnabled = " + accessibilityEnabled);
        } catch (Settings.SettingNotFoundException e) {
            Log.e(TAG, "Error finding setting, default accessibility to not found: "
                    + e.getMessage());
        }
        final TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            Log.v(TAG, "***ACCESSIBILITY IS ENABLED*** -----------------");
            String settingValue = Settings.Secure.getString(
                    _context.getApplicationContext().getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    String accessibilityService = mStringColonSplitter.next();

                    Log.v(TAG, "-------------- > accessibilityService :: " + accessibilityService + " " + service);
                    if (accessibilityService.equalsIgnoreCase(service)) {
                        Log.v(TAG, "We've found the correct setting - accessibility is switched on!");
                        return true;
                    }
                }
            }
        } else {
            Log.v(TAG, "***ACCESSIBILITY IS DISABLED***");
        }

        return false;
    }

    @Override
    public final void openAccessibilitySettings(final Context _context) {
        final Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        _context.startActivity(intent);
    }

    @Override
    public final boolean hasPhonePermission(final Context _context) {
        final int res = ContextCompat.checkSelfPermission(_context, Manifest.permission.CALL_PHONE);
        return PackageManager.PERMISSION_GRANTED == res;
    }

    @Override
    public final void requestPhonePermission(final Context _context) {
        final Intent intent = new Intent(_context, PermissionActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(intent);
    }
}
