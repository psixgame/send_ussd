package com.gmail.psixgame.sendussd;

import android.content.Context;

import com.gmail.psixgame.sendussd.balance.BalanceManagerImpl;
import com.gmail.psixgame.sendussd.balance.IBalanceManager;
import com.gmail.psixgame.sendussd.ussd.IUssdManager;
import com.gmail.psixgame.sendussd.ussd.UssdManagerImpl;
import com.gmail.psixgame.sendussd.utils.permission.IPermissionHelper;
import com.gmail.psixgame.sendussd.utils.permission.PermissionHelperImpl;
import com.gmail.psixgame.sendussd.widget.IWidgetManager;
import com.gmail.psixgame.sendussd.widget.WidgetManagerImpl;

/**
 * Created by ZOG on 5/16/2017.
 */

public final class ObjectGraph {

    private static ObjectGraph graph;

    public static ObjectGraph getInstance(final Context _context) {
        if (graph == null) {
            graph = new ObjectGraph(_context);
        }
        return graph;
    }

    private final IUssdManager mUssdManager;
    private final IBalanceManager mBalanceManager;
    private final IPermissionHelper mPermissionHelper;
    private final IWidgetManager mWidgetManager;

    private ObjectGraph(final Context _context) {
        mUssdManager = new UssdManagerImpl(_context);
        mBalanceManager = new BalanceManagerImpl(mUssdManager);
        mPermissionHelper = new PermissionHelperImpl();
        mWidgetManager = new WidgetManagerImpl(_context, mBalanceManager, mPermissionHelper);
    }

    public final IUssdManager getUssdManager() {
        return mUssdManager;
    }

    public final IPermissionHelper getPermissionHelper() {
        return mPermissionHelper;
    }

    public final IWidgetManager getWidgetManager() {
        return mWidgetManager;
    }
}
