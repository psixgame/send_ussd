package com.gmail.psixgame.sendussd.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.gmail.psixgame.sendussd.R;
import com.gmail.psixgame.sendussd.balance.IBalanceManager;
import com.gmail.psixgame.sendussd.balance.UssdBalanceData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import hugo.weaving.DebugLog;

/**
 * Created by ZOG on 6/17/2017.
 */

final class WidgetHelper {

    @DebugLog
    final int getWidgetId(final Intent _intent) {
        return _intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
    }

    @DebugLog
    final boolean isIdValid(final int _id) {
        return AppWidgetManager.INVALID_APPWIDGET_ID != _id;
    }

    @DebugLog
    final void putWidgetId(final Intent _intent, final int _id) {
        _intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, _id);
    }

    @Deprecated
    @DebugLog
    final String buildDataMoneyString(final Context _context, final UssdBalanceData _data) {
        return buildDataString(_context, _data, IBalanceManager.Key.MONEY);
    }

    @Deprecated
    @DebugLog
    final String buildDataServicesString(final Context _context, final UssdBalanceData _data) {
        return buildDataString(_context, _data, IBalanceManager.Key.MINUTES) + '\n' +
                buildDataString(_context, _data, IBalanceManager.Key.SMS) + '\n' +
                buildDataString(_context, _data, IBalanceManager.Key.MEGABYTES);
    }

    @DebugLog
    private String buildDataString(final Context _context, final UssdBalanceData _data, final String _key) {
        return getTitleForKey(_context, _key) + ": " + _data.getData(_key);
    }

    @DebugLog
    final String getTitleForKey(final Context _context, final String _key) {
        switch (_key) {
            case IBalanceManager.Key.MONEY:
                return _context.getString(R.string.title_money);
            case IBalanceManager.Key.MINUTES:
                return _context.getString(R.string.title_minutes);
            case IBalanceManager.Key.SMS:
                return _context.getString(R.string.title_sms);
            case IBalanceManager.Key.MEGABYTES:
                return _context.getString(R.string.title_megabytes);
        }
        return "";
    }

    @DebugLog
    final String getTimeString(final long _time) {
        final Date date = new Date(_time);
        final SimpleDateFormat sdf = new SimpleDateFormat("hh:mm\t\tdd/MM", Locale.getDefault());
        return sdf.format(date);
    }

    @DebugLog
    final boolean hasWidgetData(final Context _context, final int _widgetId, final String _key) {
        final File file = getFile(_context, _widgetId, _key);
        return file.exists();
    }

    @DebugLog
    @Nullable
    final UssdBalanceData readWidgetData(final Context _context, final int _widgetId, final String _key) {
        try {
            final FileInputStream fis = new FileInputStream(getFile(_context, _widgetId, _key));
            final ObjectInputStream is = new ObjectInputStream(fis);
            final UssdBalanceData data = (UssdBalanceData) is.readObject();
            is.close();
            fis.close();
            return data;
        } catch (final IOException | ClassNotFoundException _e) {
            _e.printStackTrace();
        }
        return null;
    }

    @DebugLog
    final void saveWidgetData(final Context _context, final int _widgetId, final String _key, final UssdBalanceData _data) {
        try {
            final FileOutputStream fos = new FileOutputStream(getFile(_context, _widgetId, _key));
            final ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(_data);
            os.close();
            fos.close();
        } catch (final IOException _e) {
            _e.printStackTrace();
        }
    }

    @DebugLog
    final void deleteWidgetData(final Context _context, final int _widgetId, final String _key) {
        final File file = getFile(_context, _widgetId, _key);
        file.delete();
    }

    private File getFile(final Context _context, final int _widgetId, final String _key) {
        return new File(_context.getCacheDir(), _widgetId + _key);
    }

}
