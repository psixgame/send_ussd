package com.gmail.psixgame.sendussd.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViews;

import com.gmail.psixgame.sendussd.ObjectGraph;
import com.gmail.psixgame.sendussd.R;
import com.gmail.psixgame.sendussd.balance.IBalanceManager;
import com.gmail.psixgame.sendussd.balance.UssdBalanceData;

import hugo.weaving.DebugLog;

/**
 * Created by ZOG on 1/13/2017.
 */

public final class Widget extends AppWidgetProvider {

    interface Action {
        /**
         * CLICKED actions are used in two ways:
         * - handling clicks on widget;
         * - notifying {@link WidgetManagerImpl} about that clicks.
         */
        String ACTION_CLICKED_MONEY         = "com.gmail.psixgame.sendussd.widget.Widget.Action.CLICKED_MONEY";
        String ACTION_CLICKED_SERVICES      = "com.gmail.psixgame.sendussd.widget.Widget.Action.CLICKED_SERVICES";
        /**
         * GOT_DATA actions are used in two ways:
         * - notifying widget about data receiving;
         * - as keys for getting serializable data from intents.
         * These actions are registered in manifest.
         */
        String ACTION_GOT_DATA_MONEY        = "com.gmail.psixgame.sendussd.widget.Widget.Action.GOT_DATA_MONEY";
        String ACTION_GOT_DATA_SERVICES     = "com.gmail.psixgame.sendussd.widget.Widget.Action.GOT_DATA_SERVICES";
    }

    private final WidgetHelper mHelper;

    @DebugLog
    public Widget() {
        mHelper = new WidgetHelper();
    }

    @DebugLog
    @Override
    public final void onReceive(final Context _context, final Intent _intent) {
        super.onReceive(_context, _intent);
        final int id = mHelper.getWidgetId(_intent);
        if (!mHelper.isIdValid(id)) return;

        final String action = _intent.getAction();
        switch (action) {
            case Action.ACTION_CLICKED_MONEY:
            case Action.ACTION_CLICKED_SERVICES:
                onClick(_context, _intent);
                break;

            case Action.ACTION_GOT_DATA_MONEY:
            case Action.ACTION_GOT_DATA_SERVICES:
                gotData(_context, _intent);
                break;
        }
    }

    @Override
    public final void onDeleted(final Context _context, final int[] _appWidgetIds) {
        super.onDeleted(_context, _appWidgetIds);
        for (final int id : _appWidgetIds) {
            if (mHelper.hasWidgetData(_context, id, Action.ACTION_GOT_DATA_MONEY)) {
                mHelper.deleteWidgetData(_context, id, Action.ACTION_GOT_DATA_MONEY);
            }
            if (mHelper.hasWidgetData(_context, id, Action.ACTION_GOT_DATA_SERVICES)) {
                mHelper.deleteWidgetData(_context, id, Action.ACTION_GOT_DATA_SERVICES);
            }
        }
    }

    @DebugLog
    private void onClick(final Context _context, final Intent _intent) {
        final int id = mHelper.getWidgetId(_intent);
        final String action = _intent.getAction();

        final Intent i = new Intent(action);
        mHelper.putWidgetId(i, id);
        _context.sendBroadcast(i);
    }

    @DebugLog
    private void gotData(final Context _context, final Intent _intent) {
        final int id = mHelper.getWidgetId(_intent);
        final String action = _intent.getAction();
        final UssdBalanceData data = (UssdBalanceData) _intent.getSerializableExtra(action);

        final RemoteViews views = createRemoteViews(_context, id);
        switch (action) {
            case Action.ACTION_GOT_DATA_MONEY:
                updateMoneyUi(_context, views, data);
                break;

            case Action.ACTION_GOT_DATA_SERVICES:
                updateServicesUi(_context, views, data);
                break;
        }

        AppWidgetManager.getInstance(_context).updateAppWidget(id, views);

        mHelper.saveWidgetData(_context, id, action, data);
    }

    private void updateMoneyUi(final Context _context, final RemoteViews _views, final UssdBalanceData _data) {
        _views.setTextViewText(R.id.tvHeaderMoney_WB, mHelper.getTimeString(_data.getTime()));

        if (_data.isSuccessful()) {
            _views.setTextViewText(R.id.tvValueMoney_WB, _data.getData(IBalanceManager.Key.MONEY));
        } else {
            _views.setTextViewText(R.id.tvValueMoney_WB, _context.getText(R.string.widget_error));
        }
    }

    private void updateServicesUi(final Context _context, final RemoteViews _views, final UssdBalanceData _data) {
        _views.setTextViewText(R.id.tvHeaderServices_WB, mHelper.getTimeString(_data.getTime()));

        if (_data.isSuccessful()) {
            _views.setTextViewText(R.id.tvValueMinutes_WB, _data.getData(IBalanceManager.Key.MINUTES));
            _views.setTextViewText(R.id.tvValueSms_WB, _data.getData(IBalanceManager.Key.SMS));
            _views.setTextViewText(R.id.tvValueMb_WB, _data.getData(IBalanceManager.Key.MEGABYTES));
        } else {
            _views.setTextViewText(R.id.tvValueMinutes_WB, _context.getText(R.string.widget_error));
            _views.setTextViewText(R.id.tvValueSms_WB, _context.getText(R.string.widget_error));
            _views.setTextViewText(R.id.tvValueMb_WB, _context.getText(R.string.widget_error));
        }
    }

    @DebugLog
    @Override
    public final void onUpdate(final Context _context, final AppWidgetManager _appWidgetManager, final int[] _appWidgetIds) {
        for (final int id : _appWidgetIds) {
            final RemoteViews views = createRemoteViews(_context, id);
            _appWidgetManager.updateAppWidget(id, views);
        }
    }

    @DebugLog
    private RemoteViews createRemoteViews(final Context _context, final int _widgetId) {
        final RemoteViews rv = new RemoteViews(_context.getPackageName(), R.layout.widget_balance);

        rv.setTextViewText(R.id.tvKeyMoney_WB, mHelper.getTitleForKey(_context, IBalanceManager.Key.MONEY));
        rv.setTextViewText(R.id.tvKeyMinutes_WB, mHelper.getTitleForKey(_context, IBalanceManager.Key.MINUTES));
        rv.setTextViewText(R.id.tvKeySms_WB, mHelper.getTitleForKey(_context, IBalanceManager.Key.SMS));
        rv.setTextViewText(R.id.tvKeyMb_WB, mHelper.getTitleForKey(_context, IBalanceManager.Key.MEGABYTES));

        final Intent iMoney = new Intent(_context, Widget.class);
        iMoney.setAction(Action.ACTION_CLICKED_MONEY);
        mHelper.putWidgetId(iMoney, _widgetId);
        final PendingIntent piMoney = PendingIntent.getBroadcast(_context, _widgetId, iMoney, 0);
        rv.setOnClickPendingIntent(R.id.llContainerMoney_WB, piMoney);

        if (mHelper.hasWidgetData(_context, _widgetId, Action.ACTION_GOT_DATA_MONEY)) {
            final UssdBalanceData data = mHelper.readWidgetData(_context, _widgetId, Action.ACTION_GOT_DATA_MONEY);
            updateMoneyUi(_context, rv, data);
        }

        final Intent iServices = new Intent(_context, Widget.class);
        iServices.setAction(Action.ACTION_CLICKED_SERVICES);
        mHelper.putWidgetId(iServices, _widgetId);
        final PendingIntent piServices = PendingIntent.getBroadcast(_context, _widgetId, iServices, 0);
        rv.setOnClickPendingIntent(R.id.llContainerServices_WB, piServices);

        if (mHelper.hasWidgetData(_context, _widgetId, Action.ACTION_GOT_DATA_SERVICES)) {
            final UssdBalanceData data = mHelper.readWidgetData(_context, _widgetId, Action.ACTION_GOT_DATA_SERVICES);
            updateServicesUi(_context, rv, data);
        }

        return rv;
    }
}
