package com.gmail.psixgame.sendussd.ussd;

import android.accessibilityservice.AccessibilityService;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import com.gmail.psixgame.sendussd.ObjectGraph;

/**
 * Created by ZOG on 5/16/2017.
 */

public final class UssdAccessibilityService extends AccessibilityService {

    public static final String TAG = "UssdAccessibilityServ";

    private UssdManagerImpl mUssdManager;

    @Override
    public final void onCreate() {
        super.onCreate();
        mUssdManager = (UssdManagerImpl) ObjectGraph.getInstance(getApplicationContext()).getUssdManager();
    }

    @Override
    public final void onAccessibilityEvent(final AccessibilityEvent _event) {
        Log.d(TAG, "onAccessibilityEvent");
        Log.d(TAG, "Pid=" + android.os.Process.myPid());
        final String text = _event.getText().toString();

        if (mUssdManager.isUssdRequestRunning() && _event.getClassName().equals("android.app.AlertDialog")) {
            performGlobalAction(GLOBAL_ACTION_BACK);
            Log.d(TAG, text);
            mUssdManager.gotUssdMessage(text);
        }
    }

    @Override
    public final void onInterrupt() {
        Log.d(TAG, "onInterrupt");
    }

}
