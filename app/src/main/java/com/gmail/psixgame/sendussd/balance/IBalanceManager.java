package com.gmail.psixgame.sendussd.balance;

/**
 * Created by ZOG on 6/5/2017.
 */

public interface IBalanceManager {

    interface Type {
        String MONEY = "type_money";
        String SERVICES = "type_services";
    }

    interface Key {
        String MONEY        = "money";
        String MINUTES      = "minutes";
        String SMS          = "sms";
        String MEGABYTES = "kilobytes";
    }

    void requestBalance(String _type, BalanceCallback _callback);

    interface BalanceCallback {
        void onBalanceResponse(UssdBalanceData _data);

    }
}
