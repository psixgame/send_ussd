package com.gmail.psixgame.sendussd.balance;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ZOG on 1/16/2017.
 */

public final class UssdBalanceData implements Serializable {

    private long mTime;
    //todo: remove true in production
    private boolean mSuccessful = true;
    private final Map<String, String> mData = new HashMap<>();

    public final long getTime() {
        return mTime;
    }

    public final void setTime(final long _time) {
        mTime = _time;
    }

    public final boolean isSuccessful() {
        return mSuccessful;
    }

    public final void setSuccessful(final boolean _successful) {
        mSuccessful = _successful;
    }

    public final void addData(final String _key, final String _value) {
        mData.put(_key, _value);
    }

    public final String getData(final String _key) {
        return mData.get(_key);
    }

    public final void removeData(final String _key) {
        mData.remove(_key);
    }
}
