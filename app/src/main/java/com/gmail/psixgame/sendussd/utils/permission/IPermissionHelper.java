package com.gmail.psixgame.sendussd.utils.permission;

import android.content.Context;

/**
 * Created by ZOG on 5/16/2017.
 */

public interface IPermissionHelper {

    boolean isEnabledInAccessibilitySetting(final Context _context);
    void openAccessibilitySettings(final Context _context);
    boolean hasPhonePermission(final Context _context);
    void requestPhonePermission(final Context _context);

}
