package com.gmail.psixgame.sendussd.ussd;

/**
 * Created by ZOG on 5/12/2017.
 */

public final class UssdMessage {

    private String mMessage;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(final String _message) {
        mMessage = _message;
    }

    @Override
    public String toString() {
        return "UssdMessage{" +
                "Message='" + mMessage + '\'' +
                '}';
    }
}
