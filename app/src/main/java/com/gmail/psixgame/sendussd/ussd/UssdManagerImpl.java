package com.gmail.psixgame.sendussd.ussd;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import hugo.weaving.DebugLog;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by ZOG on 5/12/2017.
 */

public final class UssdManagerImpl implements IUssdManager {

    private final Context mContext;
    private boolean mUssdRunning = false;
    private UssdCallback mUssdCallback;

    public UssdManagerImpl(final Context _context) {
        mContext = _context;
    }

    @Override
    public final boolean isUssdRequestRunning() {
        return mUssdRunning;
    }

    @Override
    public final void sendUssd(final UssdMessage _message, final UssdCallback _callback) {
        mUssdCallback = _callback;
        mUssdRunning = true;
        final String msg = _message.getMessage();
        startCallActivity(mContext, msg);
    }

    private void startCallActivity(final Context _context, final String _ussdMessage) {
        final Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + _ussdMessage));
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(intent);
    }

    @DebugLog
    final void gotUssdMessage(final String _text) {
        mUssdRunning = false;
        final UssdMessage response = new UssdMessage();
        response.setMessage(_text);
        mUssdCallback.onResponse(response);
    }

}
