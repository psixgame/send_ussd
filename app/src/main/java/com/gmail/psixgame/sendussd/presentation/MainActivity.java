package com.gmail.psixgame.sendussd.presentation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.gmail.psixgame.sendussd.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

}
