package com.gmail.psixgame.sendussd.utils.time;

import hugo.weaving.DebugLog;

/**
 * Created by ZOG on 6/26/2017.
 */

public final class TimeProviderImpl implements ITimeProvider {

    @DebugLog
    @Override
    public final long getCurrentTime() {
        return System.currentTimeMillis();
    }

}
