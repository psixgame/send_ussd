package com.gmail.psixgame.sendussd.ussd;

/**
 * Created by ZOG on 5/16/2017.
 */

public interface UssdCallback {
    void onResponse(UssdMessage _message);
}
