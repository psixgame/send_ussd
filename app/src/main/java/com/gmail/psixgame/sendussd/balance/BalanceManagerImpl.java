package com.gmail.psixgame.sendussd.balance;

import android.net.Uri;

import com.gmail.psixgame.sendussd.ussd.IUssdManager;
import com.gmail.psixgame.sendussd.ussd.UssdCallback;
import com.gmail.psixgame.sendussd.ussd.UssdMessage;

/**
 * Created by ZOG on 6/5/2017.
 */

public final class BalanceManagerImpl implements IBalanceManager {

    private static final String USSD_NUMBER_MONEY = "*101#".replaceAll("#", Uri.encode("#"));
    private static final String USSD_NUMBER_SERVICES = "*101*4#".replaceAll("#", Uri.encode("#"));

    private final IUssdManager mUssdManager;
    private String mType;
    private BalanceCallback mBalanceCallback;

    public BalanceManagerImpl(final IUssdManager _ussdManager) {
        mUssdManager = _ussdManager;
    }

    @Override
    public final void requestBalance(final String _type, final BalanceCallback _callback) {
        mType = _type;
        mBalanceCallback = _callback;
        final UssdMessage msg = new UssdMessage();
        msg.setMessage(getUssdNumber(_type));
        mUssdManager.sendUssd(msg, mUssdCallback);
    }

    private String getUssdNumber(final String _type) {
        final String msg;
        switch (_type) {
            case Type.MONEY:
                msg = USSD_NUMBER_MONEY;
                break;

            case Type.SERVICES:
            default:
                msg = USSD_NUMBER_SERVICES;
                break;
        }
        return msg;
    }

    private final UssdCallback mUssdCallback = new UssdCallback() {
        @Override
        public final void onResponse(final UssdMessage _message) {
            final UssdBalanceData data = parseUssdResponse(_message);
            mBalanceCallback.onBalanceResponse(data);
        }
    };

    private UssdBalanceData parseUssdResponse(final UssdMessage _message) {
        final UssdBalanceData data;
        switch (mType) {
            case Type.MONEY:
                data = new UssdParserImpl().parseMoney(_message.getMessage());
                break;

            case Type.SERVICES:
            default:
                data = new UssdParserImpl().parseServicesLimit(_message.getMessage());
                break;
        }
        return data;
    }

}
