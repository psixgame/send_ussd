package com.gmail.psixgame.sendussd.balance;

import com.gmail.psixgame.sendussd.utils.time.ITimeProvider;
import com.gmail.psixgame.sendussd.utils.time.TimeProviderImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ZOG on 1/16/2017.
 */

final class UssdParserImpl implements IUssdParser {

    private static final Pattern mMoneyRegex = Pattern.compile("[-+]?[0-9]*\\.?[0-9]+");
    private static final Pattern mServicesRegex = Pattern.compile("[-+]?[0-9]*\\.?[0-9]+");

    private final ITimeProvider mTimeProvider = new TimeProviderImpl();

    @Override
    public final UssdBalanceData parseMoney(final String _data) {
        final UssdBalanceData data = new UssdBalanceData();
        if (_data.contains("Na Vashomu rahunku")) {
            final List<String> numbers = new ArrayList<>();
            final Matcher matcher = mMoneyRegex.matcher(_data);
            while (matcher.find()) {
                numbers.add(matcher.group());
            }
            final String money = numbers.get(0);
            data.addData(IBalanceManager.Key.MONEY, money);
            data.setSuccessful(true);
        } else {
            data.addData(IBalanceManager.Key.MONEY, getRandomErrorValue());
        }
        addTimeToResponse(data);
        return data;
    }

    @Override
    public final UssdBalanceData parseServicesLimit(final String _data) {
        final UssdBalanceData data = new UssdBalanceData();
        if (_data.length() >= 148) {
            final List<String> numbers = new ArrayList<>();
            final Matcher matcher = mServicesRegex.matcher(_data);
            while (matcher.find()) {
                numbers.add(matcher.group());
            }
            final String minutes = numbers.get(0);
            final String sms = numbers.get(3);
            final String mb = numbers.get(5);
            data.addData(IBalanceManager.Key.MINUTES, minutes);
            data.addData(IBalanceManager.Key.SMS, sms);
            data.addData(IBalanceManager.Key.MEGABYTES, mb);
            data.setSuccessful(true);
        } else {
            data.addData(IBalanceManager.Key.MINUTES, getRandomErrorValue());
            data.addData(IBalanceManager.Key.SMS, getRandomErrorValue());
            data.addData(IBalanceManager.Key.MEGABYTES, getRandomErrorValue());
        }
        addTimeToResponse(data);
        return data;
    }

    private String getRandomErrorValue() {
        return String.format("%.2fe", new Random(System.nanoTime()).nextDouble() * 100);
    }

    private void addTimeToResponse(final UssdBalanceData _data) {
        _data.setTime(mTimeProvider.getCurrentTime());
    }

}
