package com.gmail.psixgame.sendussd.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.gmail.psixgame.sendussd.balance.IBalanceManager;
import com.gmail.psixgame.sendussd.balance.UssdBalanceData;
import com.gmail.psixgame.sendussd.utils.permission.IPermissionHelper;

/**
 * Created by ZOG on 5/26/2017.
 */

public final class WidgetManagerImpl implements IWidgetManager {

    private final Context mContext;
    private final IBalanceManager mBalanceManager;
    private final IPermissionHelper mPermissionHelper;
    private final WidgetReceiver mWidgetReceiver;
    private final WidgetHelper mHelper;

    public WidgetManagerImpl(final Context _context, final IBalanceManager _balanceManager, final IPermissionHelper _permissionHelper) {
        mContext = _context;
        mBalanceManager = _balanceManager;
        mPermissionHelper = _permissionHelper;
        mWidgetReceiver = new WidgetReceiver(this);
        mHelper = new WidgetHelper();

        registerWidgetReceiver();
    }

    private void registerWidgetReceiver() {
        mContext.registerReceiver(mWidgetReceiver, createIntentFilter());
    }

    private IntentFilter createIntentFilter() {
        final IntentFilter filter = new IntentFilter();
        filter.addAction(Widget.Action.ACTION_CLICKED_MONEY);
        filter.addAction(Widget.Action.ACTION_CLICKED_SERVICES);
        return filter;
    }

    private void gotRequestMoney(final int _id) {
        if (!canProceed(mContext)) return;

        mBalanceManager.requestBalance(IBalanceManager.Type.MONEY, new IBalanceManager.BalanceCallback() {
            @Override
            public final void onBalanceResponse(final UssdBalanceData _data) {
                gotResponseMoney(_id, _data);
            }
        });
    }

    private void gotResponseMoney(final int _id, final UssdBalanceData _data) {
        final Intent intent = new Intent(Widget.Action.ACTION_GOT_DATA_MONEY);
        mHelper.putWidgetId(intent, _id);
        intent.putExtra(Widget.Action.ACTION_GOT_DATA_MONEY, _data);
        mContext.sendBroadcast(intent);
    }

    private void gotRequestServices(final int _id) {
        if (!canProceed(mContext)) return;

        mBalanceManager.requestBalance(IBalanceManager.Type.SERVICES, new IBalanceManager.BalanceCallback() {
            @Override
            public final void onBalanceResponse(final UssdBalanceData _data) {
                gotResponseServices(_id, _data);
            }
        });
    }

    private void gotResponseServices(final int _id, final UssdBalanceData _message) {
        final Intent intent = new Intent(Widget.Action.ACTION_GOT_DATA_SERVICES);
        mHelper.putWidgetId(intent, _id);
        intent.putExtra(Widget.Action.ACTION_GOT_DATA_SERVICES, _message);
        mContext.sendBroadcast(intent);
    }

    private boolean canProceed(final Context _context) {
        if (!mPermissionHelper.isEnabledInAccessibilitySetting(_context)) {
            mPermissionHelper.openAccessibilitySettings(_context);
            return false;
        } else if (!mPermissionHelper.hasPhonePermission(_context)) {
            mPermissionHelper.requestPhonePermission(_context);
            return false;
        }
        return true;
    }

    private static final class WidgetReceiver extends BroadcastReceiver {

        private final WidgetManagerImpl mWidgetManager;

        public WidgetReceiver(final WidgetManagerImpl _widgetManager) {
            mWidgetManager = _widgetManager;
        }

        @Override
        public final void onReceive(final Context _context, final Intent _intent) {
            final int id = mWidgetManager.mHelper.getWidgetId(_intent);
            if (!mWidgetManager.mHelper.isIdValid(id)) return;

            switch (_intent.getAction()) {
                case Widget.Action.ACTION_CLICKED_MONEY:
                    mWidgetManager.gotRequestMoney(id);
                    break;

                case Widget.Action.ACTION_CLICKED_SERVICES:
                    mWidgetManager.gotRequestServices(id);
                    break;
            }
        }

    }
}
