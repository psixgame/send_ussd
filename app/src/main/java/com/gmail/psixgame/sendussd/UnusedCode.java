package com.gmail.psixgame.sendussd;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.PowerManager;
import android.provider.Settings;

/**
 * Created by ZOG on 6/30/2017.
 */

abstract class UnusedCode {

    public final boolean isAddedToBatteryWhiteList(final Context _context) {
        final PowerManager pm = (PowerManager) _context.getSystemService(Context.POWER_SERVICE);
        final String packageName = _context.getPackageName();
        return pm.isIgnoringBatteryOptimizations(packageName);
    }

    public final void openBatteryOptimizationSettings(final Context _context) {
        final Intent intent = new Intent();
        final String packageName = _context.getPackageName();
        final PowerManager pm = (PowerManager) _context.getSystemService(Context.POWER_SERVICE);
        if (pm.isIgnoringBatteryOptimizations(packageName))
            intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
        else {
            intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + packageName));
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(intent);
    }

}
