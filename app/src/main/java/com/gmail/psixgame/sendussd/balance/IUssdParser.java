package com.gmail.psixgame.sendussd.balance;

/**
 * Created by ZOG on 5/24/2017.
 */

interface IUssdParser {

    UssdBalanceData parseMoney(String _data);
    UssdBalanceData parseServicesLimit(String _data);

}
