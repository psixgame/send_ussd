package com.gmail.psixgame.sendussd;

import android.app.Application;

/**
 * Created by ZOG on 5/16/2017.
 */

public final class UssdApplication extends Application {

    @Override
    public final void onCreate() {
        super.onCreate();
        ObjectGraph.getInstance(getApplicationContext());
    }
}
