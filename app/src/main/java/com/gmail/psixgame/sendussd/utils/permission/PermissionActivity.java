package com.gmail.psixgame.sendussd.utils.permission;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

/**
 * Created by ZOG on 1/23/2017.
 */

public final class PermissionActivity extends Activity {

    private static final String TAG = PermissionActivity.class.getSimpleName();

    private static final int CODE_PHONE_PERMISSION = 1000;

    @Override
    protected final void onStart() {
        super.onStart();
        requestPhonePermission();
    }

    private final void requestPhonePermission() {
        ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.CALL_PHONE }, CODE_PHONE_PERMISSION);
    }

    @Override
    public final void onRequestPermissionsResult(final int _requestCode, final String[] _permissions, final int[] _grantResults) {
        if (CODE_PHONE_PERMISSION != _requestCode) return;

        if (_grantResults.length > 0 && _grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Permission granted");
        } else {
            Log.d(TAG, "Permission denied");
        }
        finish();
    }
}
